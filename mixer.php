<?php
require_once 'vendor/autoload.php';

use League\HTMLToMarkdown\HtmlConverter;
use Symfony\Component\Filesystem\Filesystem;

$PARAGRAPH_TYPES = [
    1 => 'REVIEW',
    2 => 'REGISTRATION',
    3 => 'SOFTWARE',
    4 => 'SLOTS',
    5 => 'TABLE_GAMES',
    6 => 'BONUSES',
    7 => 'BANKING',
    8 => 'MOBILE',
    9 => 'SECURITY',
    10 => 'SUPPORT',
    11 => 'CONCLUSION'
];

$types = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

$allowed_tags = [
//    "a",
//    "abbr",
//    "acronym",
//    "address",
//    "applet",
//    "area",
//    "article",
//    "aside",
//    "audio",
    "b",
//    "base",
//    "basefont",
//    "bdi",
//    "bdo",
//    "bgsound",
//    "big",
//    "blink",
//    "blockquote",
//    "body",
    "br",
//    "button",
//    "canvas",
//    "caption",
//    "center",
//    "cite",
//    "code",
//    "col",
//    "colgroup",
//    "content",
//    "data",
//    "datalist",
//    "dd",
//    "decorator",
//    "del",
//    "details",
//    "dfn",
//    "dir",
//    "div",
//    "dl",
//    "dt",
//    "element",
    "em",
    "embed",
//    "fieldset",
//    "figcaption",
//    "figure",
//    "font",
//    "footer",
//    "form",
//    "frame",
//    "frameset",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
//    "head",
//    "header",
//    "hgroup",
    "hr",
//    "html",
    "i",
//    "iframe",
//    "img",
//    "input",
//    "ins",
//    "isindex",
//    "kbd",
//    "keygen",
//    "label",
//    "legend",
    "li",
//    "link",
    "listing",
//    "main",
//    "map",
//    "mark",
//    "marquee",
//    "menu",
//    "menuitem",
//    "meta",
//    "meter",
//    "nav",
//    "nobr",
//    "noframes",
//    "noscript",
//    "object",
    "ol",
//    "optgroup",
//    "option",
//    "output",
    "p",
//    "param",
//    "plaintext",
    "pre",
//    "progress",
//    "q",
//    "rp",
//    "rt",
//    "ruby",
//    "s",
//    "samp",
//    "script",
//    "section",
//    "select",
//    "shadow",
    "small",
//    "source",
//    "spacer",
    "span",
//    "strike",
    "strong",
//    "style",
//    "sub",
//    "summary",
//    "sup",
//    "table",
//    "tbody",
//    "td",
//    "template",
//    "textarea",
//    "tfoot",
//    "th",
//    "thead",
//    "time",
    "title",
//    "tr",
//    "track",
//    "tt",
    "u",
    "ul",
//    "var",
//    "video",
//    "wbr",
//    "xmp"
];

$files = [];
$paragraphs = [];

// check input files
foreach (scandir('sites') as $v) {
    if (!in_array($v, ['.', '..'])) {
        $files[] = $v;
    }
}

$index = 0;
// fill all to collection
foreach ($files as $ak => $file) {
    $json = json_decode(file_get_contents("sites" . DIRECTORY_SEPARATOR . $file), true);

    foreach ($json as $k => $v) {
        if (isset($v['article'])) {

            foreach ($v['article'] as $pk => $par) {

//                print_r($par['pargraph']);
//                $tmp = preg_replace('/%u([a-fA-F0-9]{4})/', '&#x\\1;', $par['paragraph']['text']);
                $tmp = $par['paragraph']['text'];
                $context = html2md(strip_tags(urldecode($tmp), $allowed_tags)) ."\n\n";


                $paragraphs[] = [
                    'f' => $ak,
                    'a' => $index,
                    'type' => intval($par['paragraph']['type']),
                    'text' => $context
                ];

                $index++;
            }

        }
    }

}

$paragraphs = collect($paragraphs);

//print_r($paragraphs);
//exit();

//print_r($paragraphs->where('type', 1)->all());
removeFile("output" . DIRECTORY_SEPARATOR . "result.md");
$exFile = null;
$exArticle = null;

$numFiles = $paragraphs->where('type', 11)->count();
echo "\nTotal 11 count: {$numFiles}\n";

for ($num=0; $num<$numFiles; $num++){

    foreach ($types as $type) {


        if ($exFile === null) {

            $count = $paragraphs->where('type', $type)->count();
            echo "\nGenerate Block : " . $PARAGRAPH_TYPES[$type] . " from matched: {$count}\n";

            $filtered = $paragraphs->where('type', $type);
            $paragraph = $filtered->random();
        } else {
            $count = $paragraphs->where('type', $type)->whereNotIn('f', [$exFile])->whereNotIn('a', $exArticle)->count();
            echo "\nGenerate Block : " . $PARAGRAPH_TYPES[$type] . " from matched : {$count}\n";

            if (!$count) {
//                if ($paragraphs->where('type', $type)->whereNotIn('a', $exArticle)->count()){
//                    // looks last item?
//                    $paragraph = $paragraphs->where('type', $type)
//                        ->whereNotIn('a', $exArticle) // different article
//                        ->random();
//                } else {
                    if ($type == 11) break 2;
                    continue;
//                }

            } else {
                $paragraph = $paragraphs->where('type', $type)
                    ->whereNotIn('f', [$exFile]) // different file
                    ->whereNotIn('a', $exArticle) // different article
                    ->random();
            }


        }

        $exFile = $paragraph['f'];
        $exArticle[] = $paragraph['a'];

        storeFile("output" . DIRECTORY_SEPARATOR . "result{$num}.md", $paragraph['text'], true);
    }

}



//$a = collect([1,2,3,4,5]);
//print_r($a);


function html2md($html = null)
{
    $converter = new HtmlConverter(array('strip_tags' => true));
    $markdown = $converter->convert($html);

    return $markdown;
}


function storeFile($fileName = null, $context = null, $append = false)
{
    if ($fileName == null || $context == null) return;
    $filesystem = new Filesystem();

    if ($append) {
        $filesystem->appendToFile($fileName, $context);
    } else {
        $filesystem->dumpFile($fileName, $context);
    }
    return true;
}


function removeFile($name)
{
    if ($name == null) return;
    $filesystem = new Filesystem();

    $filesystem->remove($name);
}
